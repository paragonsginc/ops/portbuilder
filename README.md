# User Data

## FreeBSD Sample
```
#!/bin/sh

export PATH=/sbin:/bin:/usr/sbin:/usr/bin:/usr/local/bin:/usr/local/sbin

# Bootstrap and update pkg to ensure synchronization with the repository
env ASSUME_ALWAYS_YES=YES pkg bootstrap -f | cat
env ASSUME_ALWAYS_YES=YES pkg update -f | cat

# Install Python and determine the default version
env ASSUME_ALWAYS_YES=YES pkg install python </dev/null | cat
export PYVER=$(pkg query '%v' python | sed 's/\([[:digit:]]*\)\.\([[:digit:]]*\).*/\1\2/')

# Specify required packages and states
REQUIRED_PKGS="py${PYVER}-salt py${PYVER}-pygit2"
REQUIRED_STATES="base portbuilder"

# Install requested packages
for package in ${REQUIRED_PKGS}; do
	env ASSUME_ALWAYS_YES=YES pkg install ${package} </dev/null | cat
done

mkdir -p /usr/local/etc/salt/minion.d
cat > /usr/local/etc/salt/minion.d/masterless.conf << EOF
state_output: changes
file_client: local
gitfs_provider: pygit2
use_superseded:
  - module.run

fileserver_backend:
  - git

gitfs_remotes:
  - https://gitlab.com/paragonsginc/ops/base-config.git:
    - base: main
  - https://gitlab.com/paragonsginc/ops/portbuilder.git:
    - base: main

grains:
  formula:
    poudriere:
      data:
        device: '192.168.1.1:/' # Set this to EFS mount
EOF

# Synchronize custom modules
/usr/local/bin/salt-call saltutil.sync_all

# Apply required states
for state in ${REQUIRED_STATES}; do
	/usr/local/bin/salt-call state.apply ${state}
done

# Build ports
/usr/local/bin/buildall.sh

```
