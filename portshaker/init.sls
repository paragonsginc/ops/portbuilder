# vim: sts=2 ts=2 sw=2 et ai ft=jinja
{% from salt["slsutil.findup"](tplfile, "formula.jinja") import formula with context -%}

{% set formula_path="storage" %}
{% from salt["slsutil.findup"](formula_path, "formula.jinja") import formula as storage with context -%}

include:
  - storage


{{ formula.namespace }}.install:
  pkg.installed:
    - name: {{ formula.package }}

{{ formula.namespace }}.config:
  file.managed:
    - name: {{ formula.configfile.path | yaml_encode }}
    - source: {{ formula.configfile.source | yaml_encode }}
    - template: jinja
    - user: {{ formula.configfile.user | yaml_encode }}
    - group: {{ formula.configfile.group | yaml_encode }}
    - mode: {{ formula.configfile.mode | yaml_encode }}
    - context:
        tplfile: {{ tplfile }}
    - require:
      - pkg: {{ formula.namespace }}.install

{{ formula.namespace }}.sources:
  file.recurse:
    - name: {{ formula.sources.path | yaml_encode }}
    - source: {{ formula.sources.source | yaml_encode }}
    - clean: true
    - user: {{ formula.sources.user | yaml_encode }}
    - group: {{ formula.sources.group | yaml_encode }}
    - file_mode: {{ formula.sources.file_mode | yaml_encode }}
    - template: jinja
    - require:
      - pkg: {{ formula.namespace }}.install

{{ formula.namespace }}.cache:
  file.directory:
    - name: {{ formula.cache.path }}
    - user: {{ formula.cache.user | yaml_encode }}
    - group: {{ formula.cache.group | yaml_encode }}
    - mode: {{ formula.cache.mode | yaml_encode }}
  zfs.filesystem_present:
    - name: {{ storage.zpool.name ~ formula.cache.zrootfs }}
    - properties:
        mountpoint: {{ formula.cache.path }}
    - require:
      - zpool: {{ storage.namespace }}.zpool
      - file: {{ formula.namespace }}.cache
