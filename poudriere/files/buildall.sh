#!/usr/bin/env sh

export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

portshaker -v 2>&1 | tee -a /var/log/build.log
buildports.sh -p daily -z freeipa -r 13.0 -f /usr/local/poudriere/config/freeipa.pkglist 2>&1 | tee -a /var/log/build.log
buildports.sh -p quarterly -z freeipa -r 13.0 -f /usr/local/poudriere/config/freeipa.pkglist 2>&1 | tee -a /var/log/build.log
buildports.sh -p daily -z freeipa -r 12.2 -f /usr/local/poudriere/config/freeipa.pkglist 2>&1 | tee -a /var/log/build.log
buildports.sh -p quarterly -z freeipa -r 12.2 -f /usr/local/poudriere/config/freeipa.pkglist 2>&1 | tee -a /var/log/build.log
