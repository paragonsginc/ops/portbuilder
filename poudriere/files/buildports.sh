#!/usr/bin/env sh
set -e

usage() {
  echo "Usage: buildports -p <ports> -z <set> -r <release> -f <package-list>"
  exit 1
}

# Set defaults
CPUARCH=amd64
PTNAME=default

while getopts p:z:r:f: opt
do
  case "$opt" in
    p) PTNAME=$OPTARG;;
    z) SETNAME=$OPTARG;;
    r) RELEASE=$OPTARG;;
    f) PKGLIST=$OPTARG;;
    \?) echo "Usage: buildports -p <ports> -z <set> -r <release> -f <package-list>"
  esac
done

[ "$RELEASE" = "" ] && usage
[ "$CPUARCH" = "" ] && usage
[ "$PTNAME" = "" ] && usage
[ "$SETNAME" = "" ] && usage
[ "$PKGLIST" = "" ] && usage

# Define jail name
RELMAJ=$(echo $RELEASE | cut -f1 -d.)
RELMIN=$(echo $RELEASE | cut -f2 -d.)
JAILNAME=freebsd-$RELMAJ-$RELMIN-$CPUARCH

echo "########################################################################"
echo "Building ports for ${JAILNAME}-${PTNAME}-${SETNAME}"
echo "########################################################################"

# Verify the ports tree exists
if ! poudriere ports -lq | cut -f 1 -w | grep -Fxq "${PTNAME}";
then

  # Exit if the directory is not present
  if [ ! -d /usr/local/poudriere/ports/$PTNAME ]
  then
    echo "Ports tree $PTNAME is not present"
    exit 1
  fi

  # Create the ports tree
  poudriere ports -c -m null -p $PTNAME -M /usr/local/poudriere/ports/$PTNAME
fi

# Create or update jail
if [ -d /usr/local/etc/poudriere.d/jails/$JAILNAME ]
then
  # Kill the jail if it is running
  poudriere -N jail -k -j $JAILNAME -p $PTNAME -z $SETNAME
else
  # Create the jail
  poudriere -N jail -c -j $JAILNAME -v $RELEASE-RELEASE -a $CPUARCH
fi

# Update jail
poudriere -N jail -j $JAILNAME -u

# Build packages
poudriere -N bulk -z $SETNAME -p $PTNAME -f $PKGLIST -j $JAILNAME
