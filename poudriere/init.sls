# vim: sts=2 ts=2 sw=2 et ai ft=jinja
{% from salt["slsutil.findup"](tplfile, "formula.jinja") import formula with context -%}

{% set formula_path="storage" %}
{% from salt["slsutil.findup"](formula_path, "formula.jinja") import formula as storage with context -%}

include:
  - storage
  - nfs.client


{{ formula.namespace }}.install:
  pkg.installed:
    - name: {{ formula.package | yaml_encode }}


{{ formula.namespace }}.config:
  file.managed:
    - name: {{ formula.configfile.path | yaml_encode }}
    - source: {{ formula.configfile.source | yaml_encode }}
    - template: jinja
    - user: {{ formula.configfile.user | yaml_encode }}
    - group: {{ formula.configfile.group | yaml_encode }}
{% if grains.os_family != "Windows" %}
    - mode: {{ formula.configfile.mode | yaml_encode }}
{% endif %}
    - context:
        tplfile: {{ tplfile }}
    - require:
      - pkg: {{ formula.namespace }}.install



{% for portset in formula.portsets -%}

{{ formula.namespace }}.{{ portset }}-make-conf:
  file.managed:
    - name: /usr/local/etc/poudriere.d/{{ portset }}-make.conf
    - source: {{ formula.url ~ "portsets/" ~ portset ~ "/make.conf" }}
    - require:
      - pkg: {{ formula.namespace }}.install

{{ formula.namespace }}.{{ portset }}-options:
  file.recurse:
    - name: /usr/local/etc/poudriere.d/{{ portset }}-options
    - source: {{ formula.url ~ "portsets/" ~ portset ~ "/options" }}
    - clean: true
    - require:
      - pkg: {{ formula.namespace }}.install

{{ formula.namespace }}.{{ portset }}-pkglist:
  file.managed:
    - name: /usr/local/poudriere/config/{{ portset }}.pkglist
    - source: {{ formula.url ~ "portsets/" ~ portset ~ "/pkglist" }}
    - makedirs: true
    - require:
      - pkg: {{ formula.namespace }}.install

{% endfor %}


{{ formula.namespace }}.base-dataset:
  zfs.filesystem_present:
    - name: {{ storage.zpool.name ~ formula.config.zrootfs }}
    - properties:
        mountpoint: none
    - require:
      - zpool: {{ storage.namespace }}.zpool


{{ formula.namespace }}.jails-dataset:
  zfs.filesystem_present:
    - name: {{ storage.zpool.name ~ formula.config.zrootfs ~ "/jails" }}
    - properties:
        mountpoint: none
    - require:
      - zfs: {{ formula.namespace }}.base-dataset


{{ formula.namespace }}.ports-dataset:
  zfs.filesystem_present:
    - name: {{ storage.zpool.name ~ formula.config.zrootfs ~ "/ports" }}
    - properties:
        mountpoint: none
    - require:
      - zfs: {{ formula.namespace }}.base-dataset


{{ formula.namespace }}.distfiles-dataset:
  zfs.filesystem_present:
    - name: {{ storage.zpool.name ~ formula.config.zrootfs ~ "/distfiles" }}
    - properties:
        mountpoint: {{ formula.config.distfiles_cache }}
    - require:
      - zfs: {{ formula.namespace }}.base-dataset


{{ formula.namespace }}.ccache-install:
  pkg.installed:
    - name: ccache


{{ formula.namespace }}.poudriere-basefs:
  file.directory:
    - name: {{ formula.config.basefs }}


{{ formula.namespace }}.poudriere-data:
  file.directory:
    - name: {{ formula.config.basefs | path_join("data") }}
    - require:
      - file: {{ formula.namespace }}.poudriere-basefs
  mount.mounted:
    - name: {{ formula.config.basefs | path_join("data") }}
    - device: {{ formula.data.device | yaml_encode }}
    - fstype: {{ formula.data.fstype | yaml_encode }}
    - opts:
{{ formula.data.opts | yaml(false) | indent(6, true) }}
    - hidden_opts:
{{ formula.data.opts | yaml(false) | indent(6, true) }}
    - require:
      - file: {{ formula.namespace }}.poudriere-data
      - service: nfs.client-service
      - module: nfs.client-service


{{ formula.namespace }}.build-script:
  file.managed:
    - name: {{ formula.build_script.path | yaml_encode }}
    - source: {{ formula.build_script.source | yaml_encode }}
    - template: jinja
    - user: {{ formula.build_script.user | yaml_encode }}
    - group: {{ formula.build_script.group | yaml_encode }}
{% if grains.os_family != "Windows" %}
    - mode: {{ formula.build_script.mode | yaml_encode }}
{% endif %}
    - context:
        tplfile: {{ tplfile }}


{{ formula.namespace }}.build-all:
  file.managed:
    - name: {{ formula.build_all.path | yaml_encode }}
    - source: {{ formula.build_all.source | yaml_encode }}
    - template: jinja
    - user: {{ formula.build_all.user | yaml_encode }}
    - group: {{ formula.build_all.group | yaml_encode }}
{% if grains.os_family != "Windows" %}
    - mode: {{ formula.build_all.mode | yaml_encode }}
{% endif %}
    - context:
        tplfile: {{ tplfile }}
